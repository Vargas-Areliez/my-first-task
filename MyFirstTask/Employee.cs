﻿namespace MyFirstTask
{
    internal class Employee
    {
        public string FullName { get; set; }

        public string CI { get; set; }

        public Employee(string fullname, string CI)
        {
            FullName = fullname;
            this.CI = CI;
        }
    }
}