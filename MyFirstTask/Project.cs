﻿namespace MyFirstTask
{
    internal class Project
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Manager Manager { get; set; }
        public bool state { get; set; }

        public Project(string name, string description, Manager manager)
        {
            Name = name;
            Description = description;
            Manager = manager;
            state = false;
        }

        public bool ChangeState(bool stateActual)
        {
            if (!stateActual.Equals(state))
                state = stateActual;
            return state;
        }
    }
}