﻿using System;

namespace MyFirstTask
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Card AreliezCard = new Card(123);
            Manager Areliez = new Manager("Areliez Vargas", "123456", AreliezCard);

            Console.WriteLine(Areliez.FullName,Areliez.CI);
        }
    }
}